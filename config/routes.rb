Rails.application.routes.draw do
  root 'todo#index'
  get 'todo/update'
  post 'todo/create'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
