$(document).ready(function() {
  $("#add-todo").click(function(event) {
    event.preventDefault();
    $("#create-todo").show();
  });

  $("#cancel-btn").click(function(event) {
    event.preventDefault();
    $("#create-todo").hide();
  });
});
