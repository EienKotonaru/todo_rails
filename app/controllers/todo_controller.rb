class TodoController < ApplicationController
  def index
    @projects = Project.all
  end

  def update
  end

  def create
    @todo = Todo.new(params.require(:todo).permit(:text, :project_id))
    print @todo
    @todo.save
    redirect_to(:root)
  end
end
